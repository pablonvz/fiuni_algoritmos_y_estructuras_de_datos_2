/* dict.h   */

/* definition of the dictionary datastructure and the */
/* operations on them                                 */
/* Caution : These are very inefficient datastructures*/
/* and you must replace these by better ones          */
#include <stdio.h>
/* declare the dictionary to be pointer to the dictionary */
/* type */
typedef struct  DICT *DICT_TYPE; 

/* FUNCTIONS */


/* initialize the dictionary by inserting characters from 0 to      */
/* NO_CHAR into it. */
/* also initialize buffer character */
DICT_TYPE init_dict(int BPC);

/* This procedure inserts an element in the array having code value */ 
/* next_code and string value str                                   */ 
/* It returns 0 if the dictionary is full and so the insert cannot  */ 
/* made. Otherwise, it returns 1                                    */ 

int insert_dict (char *str, DICT_TYPE dict);
/* This procedure searches the dictionary for a string */ 
/* If the string is present, it returns its code value */ 
/* or else it returns -1                               */ 

int dict_lookup(char *str, DICT_TYPE dict);
/* This procedure searches the dictionary for an item  */   
/* whose code value is equal to the given code. If it  */ 
/* is found, then the procedure returns the associated */ 
/* string, else it returns NULL */ 

void dict_string_lookup(int code, char *curr_string, DICT_TYPE dict);

/* This procedure takes as input the file pointer and  */
/* finds   the largest string in the file which is     */
/* present in the dictionary followed by one more      */
/* character of the file. It buffers this character in */
/* a variable buffer_char because it will need this    */
/* character next time when this procedure is called   */
/* it returns the number of bytes read. Also, there is */ 
/* a parameter finished which is set to 1 if the       */ 
/* procedure encounters EOF while scanning the input.  */ 
/* Else, this is set to 0. Caution : This is a highly  */ 
/* inefficient algorithm                               */ 
/* The longest match is contained in the actual parameter */ 
/* string[] */ 

int get_longest_match(FILE *in, char *string,int *finished, DICT_TYPE dict);

