/* bits.h
 * 
 * Header file for bitstream.c.  Routines provide basic bitstream operations
 */

/* Comments:
 *
 * In this file  are the basic operations for reading and writing codes of a 
 * given length(in bits).  "bpc" is an abbreviation for bits per code.    
 * These routines only work for values of bpc>=8 and bpc<32.  They will fail
 * if these conditions do not hold.
 *
 */

#ifndef __BITSTREAM_H
#define __BITSTREAM_H 

#define CODE unsigned int     /* Type to use for CODE */
#define LAST_CODE 0xFFFFFFFF  /* Send to putCode to flush output buffer */ 


/* Returns the next bpc bits from stdin in the lower bpc bits of the return
 * value.  Higher bits are guaranteed to be zeroed.
 */
CODE getCode(int bpc,FILE *inStream);


/* Puts the lowest bpc bits of c to stdout. To make sure the last CODE gets
 * output you must send LAST_CODE as the last output before you close the 
 * stream.(i.e. call putCode(LAST_CODE,bpc) AFTER you are done outputting.)
 */
void putCode(CODE c, int bpc,FILE *outStream);

/* Returns the number of bytes Written out since the last reset */
int bytesWritten(void);

/* Returns the number of bytes Read since the last reset */
int bytesRead(void);

/* Resets the two counters. */
void reset(void);

#endif

