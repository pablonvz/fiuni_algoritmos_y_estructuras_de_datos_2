#ifndef DEFINE_GRAFO_H
#define DEFINE_GRAFO_H
/* Include guard DEFINE_GRAFO_H previene que el archivo .h 
 se incluya mas de una vez */

#include "boolean.h"

/*=========================================================================*/
/* GRAFOS */

/* Tipo de dato opaco */
typedef void Grafo;

/* Esta implementacion supone que el grafo contiene nodos numerados
 de 0 hasta num_nodos (excluyendo num_nodos). Es decir si creas
 el grafo con el numero 3, habran los nodos: 0, 1, y 2
 */
Grafo* grafo_crear(unsigned long vertex_count);

/*
 Devuelve el numero de vertices 
 */
unsigned long grafo_num_vertices(Grafo* g);

/* Devuelve el numero de aristas
 
 NOTA: en esta implementacion la aristas no son dirigidas
 asi que (2,8) es igual a (8,2)
 */
unsigned long grafo_num_aristas(Grafo* g);

/* 
 Agrega una nueva arista.

 NOTA: en esta implementacion la aristas no son dirigidas
 asi que (2,8) es igual a (8,2)
 */
Grafo* grafo_agregar_arista(Grafo* g, unsigned long vertice1,
		unsigned long vertice2, int peso);

/* Retorna el peso de esta arista 

 NOTA: en esta implementacion la aristas no son dirigidas
 asi que (2,8) es igual a (8,2)


 */
int grafo_peso(Grafo* g, unsigned long vertice1, unsigned long vertice2);

/* Destruye el grafo (libera la memoria)
 Despues de esto el puntero no debe ser usado mas 

 Devuevle FALSE si falla, TRUE si tiene exito

 */
BOOLEAN grafo_destruir(Grafo* g);

/*=========================================================================*/
/* Iteradores para grafos */

typedef void Iterador;
/*
 Devuelve un iterador para obtener el siguiente vertice adyacente

 NOTA: Al terminar de usar el iterador hay que destruirlo.
 */
Iterador* grafo_adyacentes(Grafo* g, unsigned long vertex);

/*
 Destruye el iterador (liberando memoria)
 */
void giter_destruir(Iterador* iter);

/*
 Devuelve TRUE si hay mas datos, FALSE si no 
 */
BOOLEAN giter_mas(Iterador* iter);

/*
 Devuelve el numero de vertice 
 */
unsigned long giter_vertice(Iterador* iter);

/*
 Devuelve el peso
 */
int giter_peso(Iterador* iter);

/*
 Hace que el iterador se mueva. Al igual que giter_mas() devuelve
 si hay mas datos o no
 */
BOOLEAN giter_sig(Iterador* iter);

#endif

