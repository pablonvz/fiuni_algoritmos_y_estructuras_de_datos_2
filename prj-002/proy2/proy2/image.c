#ifdef _WIN32 
#define _CRT_SECURE_NO_DEPRECATE 
#endif 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "image.h"
#include "confirm.h"

/*
 Codigo adaptado de proyecto de MIT Opencourseware Introduction to Computer Graphics

 */

typedef struct _Image {
	int width;
	int height;
	Color* data;
} IMG;

/* create an image */
Image* CreateImage(int height, int width) {
	IMG* img = (IMG*) malloc(sizeof(struct _Image));
	CONFIRM_RETVAL(img, NULL);

	img->height = height;
	img->width = width;

	img->data = (Color*) malloc(sizeof(struct _Color) * height * width);
	CONFIRM_RETVAL(img->data, NULL);

	return img;
}

/* destroy an image */
BOOLEAN DestroyImage(Image* image) {
	IMG* img = (IMG*) image;

	/* accept nulls for convenience */
	if (NULL == img)
		return TRUE;

	if (img->data) {
		free(img->data);
	}

	free(img);

	return TRUE;
}

/* some helper functions for save & load */
unsigned char ReadByte(FILE *file) {
	unsigned char b;
	int success = fread((void*) &b, sizeof(unsigned char), 1, file);
	assert(success == 1);
	return b;
}
void WriteByte(FILE *file, unsigned char b) {
	int success = fwrite((void*) &b, sizeof(unsigned char), 1, file);
	assert(success == 1);
}
unsigned char ClampColorComponent(float c) {
	int tmp;
	tmp = (int) c * 255;
	if (tmp < 0)
		tmp = 0;
	if (tmp > 255)
		tmp = 255;
	return (unsigned char) tmp;
}
/* ACCESSORS */
int Width(Image* image) {
	IMG* img = (IMG*) image;
	CONFIRM_RETVAL(img, -1);
	return img->width;
}
int Height(Image* image) {
	IMG* img = (IMG*) image;
	CONFIRM_RETVAL(img, -1);
	return img->height;
}
Color* GetPixel(Image* image, int x, int y) {
	IMG* img = (IMG*) image;
	Color *p = NULL;

	CONFIRM_RETVAL(img, NULL);

	assert(x >= 0 && x < img->width);
	assert(y >= 0 && y < img->height);
	p = &img->data[y * img->width + x];
	return p;
}

/* MODIFIERS */
void SetPixel(Image* image, int x, int y, Color* color) {
	IMG* img = (IMG*) image;
	int i = 0;

	CONFIRM_RETURN(img);

	assert(x >= 0 && x < img->width);
	assert(y >= 0 && y < img->height);
	i = y * img->width + x;
	assert(i < img->width * img->height);
	img->data[i] = *color;
}
void SetAllPixels(Image* image, Color* color) {
	IMG* img = (IMG*) image;
	int i = 0;

	CONFIRM_RETURN(img);

	for (i = 0; i < img->width * img->height; i++) {
		img->data[i] = *color;
	}
}
void DrawStraightLine(Image* image, int x0, int y0, int x1, int y1, Color* color) {
	IMG* img = (IMG*) image;
	unsigned short x_axis = 0;
	int from = 0;
	int to = 0;
	int idx = 0;

	CONFIRM_RETURN(img);

	assert(x0 >= 0 && x0 < img->width);
	assert(y0 >= 0 && y0 < img->height);

	assert(x1 >= 0 && x1 < img->width);
	assert(y1 >= 0 && y1 < img->height);

	assert(x0 == x1 || y0 == y1);
	assert(x0 <= x1 && y0 <= y1);

	if (x0 == x1) {
		x_axis = 0;
		from = y0;
		to = y1;
	} else {
		x_axis = 1;
		from = x0;
		to = x1;
	}

	for (idx = from; idx <= to; idx++) {
		int i = 0;

		if (x_axis)
			i = y0 * img->width + idx;
		else
			i = idx * img->width + x0;

		assert(i < img->width * img->height);

		img->data[i] = *color;
	}

}

/* ====================================================================
 * ====================================================================
 * Save and Load data type 2 Targa (.tga) files
 * (uncompressed, unmapped RGB images)
 */
void SaveTGA(Image* image, char* filename) {
	IMG* img = (IMG*) image;

	int i = 0, y = 0, x = 0;
	char* ext = NULL;
	FILE* file = NULL;
	Color* v = NULL;

	CONFIRM_RETURN(image);
	CONFIRM_RETURN(filename);
	CONFIRM_RETURN(strlen(filename) > 0);

	/* must end in .tga */
	ext = &filename[strlen(filename) - 4];
	assert(!strcmp(ext, ".tga"));
	file = fopen(filename, "wb");
	/* misc header information */
	for (i = 0; i < 18; i++) {
		if (i == 2)
			WriteByte(file, 2);
		else if (i == 12)
			WriteByte(file, (img->width) % 256);
		else if (i == 13)
			WriteByte(file, (img->width) / 256);
		else if (i == 14)
			WriteByte(file, (img->height) % 256);
		else if (i == 15)
			WriteByte(file, (img->height) / 256);
		else if (i == 16)
			WriteByte(file, 24);
		else if (i == 17)
			WriteByte(file, 32);
		else
			WriteByte(file, 0);
	}
	/* the data
	 * flip y so that (0,0) is bottom left corner */
	for (y = img->height - 1; y >= 0; y--) {
		for (x = 0; x < img->width; x++) {
			v = GetPixel(image, x, y);
			/* note reversed order: b, g, r */
			WriteByte(file, ClampColorComponent((float) v->b));
			WriteByte(file, ClampColorComponent((float) v->g));
			WriteByte(file, ClampColorComponent((float) v->r));
		}
	}
	fclose(file);
}
Image* LoadTGA(char *filename) {

	FILE *file = NULL;
	char *ext = NULL;
	int width = 0, height = 0, i = 0, x = 0, y = 0;
	IMG* answer = NULL;
	Color* color = NULL;
	unsigned char r = 0, g = 0, b = 0, tmp = 0;

	CONFIRM_RETVAL(filename, NULL);
	CONFIRM_RETVAL(strlen(filename) > 0, NULL);

	/* must end in .tga */
	ext = &filename[strlen(filename) - 4];
	assert(!strcmp(ext, ".tga"));
	file = fopen(filename, "rb");
	/* misc header information */
	width = 0;
	height = 0;
	for (i = 0; i < 18; i++) {
		tmp = ReadByte(file);
		if (i == 2)
			assert(tmp == 2);
		else if (i == 12)
			width += tmp;
		else if (i == 13)
			width += 256 * tmp;
		else if (i == 14)
			height += tmp;
		else if (i == 15)
			height += 256 * tmp;
		else if (i == 16)
			assert(tmp == 24);
		else if (i == 17)
			assert(tmp == 32);
		else
			assert(tmp == 0);
	}
	/* the data */
	answer->width = width;
	answer->height = height;
	/* flip y so that (0,0) is bottom left corner */
	for (y = height - 1; y >= 0; y--) {
		for (x = 0; x < width; x++) {
			/* note reversed order: b, g, r */
			b = (int) ReadByte(file);
			g = (int) ReadByte(file);
			r = (int) ReadByte(file);
			color->r = (int) (r / 255.0);
			color->g = (int) (g / 255.0);
			color->b = (int) (b / 255.0);
			SetPixel(answer, x, y, color);
		}
	}
	fclose(file);
	return answer;
}

/* ====================================================================
 * ====================================================================
 * Save and Load PPM image files using magic number 'P6'
 * and having one comment line */

void SavePPM(Image* image, char *filename) {
	IMG* img = (IMG*) image;

	char* ext = NULL;
	FILE* file = NULL;
	Color* color = NULL;
	int x = 0, y = 0;

	CONFIRM_RETURN(image);
	CONFIRM_RETURN(filename);
	CONFIRM_RETURN(strlen(filename) > 0);

	/* must end in .ppm */
	ext = &filename[strlen(filename) - 4];
	assert(!strcmp(ext, ".ppm"));
	file = fopen(filename, "w");
	/* misc header information */
	assert(file != NULL);
	fprintf(file, "P6\n");
	fprintf(file, "# Creator: Image::SavePPM()\n");
	fprintf(file, "%d %d\n", img->width, img->height);
	fprintf(file, "255\n");
	/* the data
	 * flip y so that (0,0) is bottom left corner */
	for (y = img->height - 1; y >= 0; y--) {
		for (x = 0; x < img->width; x++) {
			color = GetPixel(image, x, y);
			fputc(ClampColorComponent((float) color->r), file);
			fputc(ClampColorComponent((float) color->g), file);
			fputc(ClampColorComponent((float) color->b), file);
		}
	}
	fclose(file);
}

Image* LoadPPM(char *filename) {

	char* ext = NULL;
	FILE* file = NULL;
	Color color;

	unsigned char r = 0, g = 0, b = 0;
	int x = 0, y = 0, height = 0, width = 0;
	char tmp[100];
	IMG* answer = NULL;

	color.r = 0;
	color.g = 0;
	color.b = 0;
	memset(tmp, 100, sizeof(char));

	CONFIRM_RETVAL(filename, NULL);
	CONFIRM_RETVAL(strlen(filename) > 0, NULL);

	/* must end in .ppm */
	ext = &filename[strlen(filename) - 4];
	assert(!strcmp(ext, ".ppm"));
	file = fopen(filename, "rb");
	/* misc header information */
	width = 0;
	height = 0;
	fgets(tmp, 100, file);
	assert(strstr(tmp, "P6"));
	fgets(tmp, 100, file);
	assert(tmp[0] == '#');
	fgets(tmp, 100, file);
	sscanf(tmp, "%d %d", &width, &height);
	fgets(tmp, 100, file);
	assert(strstr(tmp, "255"));
	/* the data */
	answer->width = width;
	answer->height = height;
	/* flip y so that (0,0) is bottom left corner */
	for (y = height - 1; y >= 0; y--) {
		for (x = 0; x < width; x++) {
			r = fgetc(file);
			g = fgetc(file);
			b = fgetc(file);
			color.r = (int) (r / 255.0);
			color.g = (int) (g / 255.0);
			color.b = (int) (b / 255.0);
			SetPixel(answer, x, y, &color);
		}
	}
	fclose(file);
	return answer;

}
