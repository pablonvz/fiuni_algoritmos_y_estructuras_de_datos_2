#ifndef DEFINE_PQ_H
#define DEFINE_PQ_H

#define PRIORITY_MINOR 1
#define PRIORITY_GREATER 2

typedef void* pq_t;

pq_t pq_create(unsigned long cap, unsigned short ppref);

pq_t pq_create_from_array(unsigned short ppref, unsigned long size,
		void** values, unsigned long* priorities);

unsigned short pq_enqueue();

void* pq_dequeue(pq_t pq);

unsigned long pq_size(pq_t pq);

void pq_destroy(pq_t pq, void (*pq_node_data_destroy)(void*));

#endif

