/*	image.h	*/

#ifndef DEFINE_IMAGE_H
#define DEFINE_IMAGE_H

#include "boolean.h"

typedef struct _Color {
	int r;
	int g;
	int b;
} Color;

/* tipo de dato opaco */
typedef void Image;

/* create an image */
Image* CreateImage(int height, int width);

/* destroy an image */
BOOLEAN DestroyImage(Image* image);

/* some helper functions for save & load */
unsigned char ReadByte(FILE* file);
void WriteByte(FILE* file, unsigned char b);
unsigned char ClampColorComponent(float c);

/* ACCESSORS */
int Width(Image* image);
int Height(Image* image);
Color* GetPixel(Image* image, int x, int y);

/* MODIFIERS */
void SetPixel(Image* image, int x, int y, Color* color);
void SetAllPixels(Image* image, Color* color);
void DrawStraightLine(Image* image, int x0, int y0, int x1, int y1, Color* color);

/* LOAD & SAVE */
void SaveTGA(Image* image, char *filename);
Image* LoadTGA(char *filename);
void SavePPM(Image* image, char *filename);
Image* LoadPPM(char *filename);

#endif

