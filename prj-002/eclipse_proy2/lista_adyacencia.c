#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "boolean.h"
#include "confirm.h"
#include "grafo.h"

/*=========================================================================*/
/* GRAFOS */

typedef struct _node_t {
	struct _node_t* next;
	unsigned long id;
	int weight;
} node_t;

typedef struct _ListaAdyacencia {
	unsigned long vertex_count;
	unsigned long edge_count;
	node_t** nodes;

/* Define aqui lo que necesitas para la lista */
/* Usa coercion para convertir de Grafo* a LA* */

} LA;

/* Esta implementacion supone que el grafo contiene nodos numerados
 * de 0 hasta num_nodos (excluyendo num_nodos). Es decir si creas
 * el grafo con el numero 3, habran los nodos: 0, 1, y 2 */
Grafo* grafo_crear(unsigned long vertex_count) {
	LA* al = malloc(sizeof(LA));
	node_t** nodes = malloc(sizeof(node_t*) * vertex_count);

	if (NULL == al || NULL == nodes) {
		printf("Can not allocate memory to create graph");

		free(al);
		free(nodes);
	} else {
		memset(al, 0, sizeof(LA));
		memset(nodes, 0, sizeof(node_t*) * vertex_count);

		al->nodes = nodes;
		al->vertex_count = vertex_count;
		al->edge_count = 0;
	}

	return (Grafo*) al;
}

/* Devuelve el numero de vertices */
unsigned long grafo_num_vertices(Grafo* g) {
	return NULL == g ? 0 : ((LA*) g)->vertex_count;
}

/* Devuelve el numero de aristas
 * NOTA: en esta implementacion la aristas no son dirigidas
 * asi que (2,8) es igual a (8,2) */
unsigned long grafo_num_aristas(Grafo* g) {
	return NULL == g ? 0 : ((LA*) g)->edge_count;
}

node_t* __create_node() {
	node_t* node = malloc(sizeof(node_t));

	if (NULL == node) {
		printf("Can not allocate memory to create new edge.");
		return NULL;
	}

	memset(node, 0, sizeof(node_t));
	return node;
}

#define FLAG_FIND_NODE_OR_CREATE 0b00000001
#define MASK_FIND_NODE_OR_CREATE 0b11111110

#define FLAG_FIND_NODE_NO_FLAG 0b00000000
#define FLAG_ACTIVATED 0b11111111

node_t* __find_node(LA* al, unsigned long v0, unsigned long v1, char flags) {
	node_t* node = NULL;
	unsigned long vfrom = --v0;
	unsigned long vto = --v1;

	if (vfrom > vto) {
		vfrom = v1;
		vto = v0;
	}

	if (NULL != al->nodes[vfrom]) {
		node = al->nodes[vfrom];

		while (NULL != node) {
			if (vto == node->id)
				break;

			node = node->next;
		}
	}

	if (NULL == node && (flags | MASK_FIND_NODE_OR_CREATE) == FLAG_ACTIVATED) {
		node = __create_node();

		CONFIRM_RETVAL(NULL != node, NULL);

		if (NULL == al->nodes[vfrom]) {
			al->nodes[vfrom] = node;
		} else {
			node_t* head = al->nodes[vfrom];

			while (NULL != head->next)
				head = head->next;

			head->next = node;
		}

		node->id = vto;
	}

	return node;
}

/* Agrega una nueva arista.
 * NOTA: en esta implementacion la aristas no son dirigidas
 * asi que (2,8) es igual a (8,2) */
Grafo* grafo_agregar_arista(Grafo* g, unsigned long vertice1,
		unsigned long vertice2, int peso) {
	node_t* node = NULL;
	LA* al = (LA*) g;

	CONFIRM_RETVAL(NULL != al, NULL);
	CONFIRM_RETVAL(0 < vertice1, NULL);
	CONFIRM_RETVAL(0 < vertice2, NULL);

	node = __find_node(al, vertice1, vertice2, FLAG_FIND_NODE_OR_CREATE);
	node->weight = peso;

	al->edge_count++;
	return g;
}

/* Retorna el peso de esta arista 
 * NOTA: en esta implementacion la aristas no son dirigidas
 * asi que (2,8) es igual a (8,2) */
int grafo_peso(Grafo* g, unsigned long v0, unsigned long v1) {
	node_t* node = __find_node((LA*) g, v0, v1, FLAG_FIND_NODE_NO_FLAG);
	return NULL == node ? 0 : node->weight;
}

/* Destruye el grafo (libera la memoria)
 * Despues de esto el puntero no debe ser usado mas
 * Devuelve FALSE si falla, TRUE si tiene exito */
void __destroy_linked_list(node_t* node) {
	node_t* head = NULL;

	while (NULL != node) {
		head = node;
		node = node->next;
		free(head);
	}
}

BOOLEAN grafo_destruir(Grafo* g) {
	LA* al = (LA*) g;

	if (NULL == al)
		return TRUE;

	while (al->vertex_count > 0)
		__destroy_linked_list(al->nodes[--al->vertex_count]);

	return FALSE;
}

/*=========================================================================*/
/* Iteradores para grafos */

typedef struct _Iterador {
	node_t* head;
	node_t* cursor;
} ITER;

/* Devuelve un iterador para obtener el siguiente vertice adyacente
 * NOTA: Al terminar de usar el iterador hay que destruirlo. */
Iterador* grafo_adyacentes(Grafo* g, unsigned long vertex) {
	ITER* it = NULL;

	CONFIRM_RETVAL(NULL != g, NULL);

	it = malloc(sizeof(ITER));

	if (NULL != it) {
		memset(it, 0, sizeof(ITER));
		it->head = ((LA*) g)->nodes[vertex - 1];
		it->cursor = it->head;

		if (NULL == it->head) {
			free(it);
			return NULL;
		}
	}

	return it;
}

/* Destruye el iterador (liberando memoria) */
void giter_destruir(Iterador* iter) {
	free(iter);
}

/* Devuelve TRUE si hay mas datos, FALSE si no */
BOOLEAN giter_mas(Iterador* iter) {
	ITER* it = (ITER*) iter;

	CONFIRM_RETVAL(NULL != it, FALSE);

	if (NULL != it->cursor->next)
		return TRUE;
	else
		return FALSE;
}

/* Devuelve el numero de vertice */
unsigned long giter_vertice(Iterador* iter) {
	ITER* it = (ITER*) iter;

	CONFIRM_RETVAL(it, -1);
	return it->cursor->id + 1;
}

/* Devuelve el peso */
int giter_peso(Iterador* iter) {
	ITER* it = (ITER*) iter;

	CONFIRM_RETVAL(it, 0);
	return it->cursor->weight;
}

/* Hace que el iterador se mueva. Al igual que giter_mas()
 * devuelve si hay mas datos o no */
BOOLEAN giter_sig(Iterador* iter) {
	ITER* it = (ITER*) iter;

	CONFIRM_RETVAL(it, FALSE);

	if (giter_mas(iter)) {
		it->cursor = it->cursor->next;
		return TRUE;
	}

	return FALSE;
}

