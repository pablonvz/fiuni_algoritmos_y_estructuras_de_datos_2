/* Proyecto 2: El Laberinto



 Nombre del Alumno: _______________


 TRABAJO INDIVIDUAL: no compartir soluciones/archivos!!

 */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "cfg_parameters.h"
#include "confirm.h"
#include "grafo.h"
#include "image.h"
#include "pq.h"

void print_graph(Grafo* graph) {
	unsigned long vidx = 0;
	unsigned long vertex_count = grafo_num_vertices(graph);

	for (vidx = 1; vidx <= vertex_count; vidx++) {
		Iterador* it = grafo_adyacentes(graph, vidx);

		printf("_%lu\t", vidx);

		if (it) {
			do {
				printf("%lu:%i\t", giter_vertice(it), giter_peso(it));
			} while (giter_sig(it));
			giter_destruir(it);
		}

		printf("\n");
	}

	printf("\n");
}

void __vertex_pq_data_destroy(void* data) {
	free(data);
}

Grafo* prim(Grafo* graph) {
	unsigned long current_vertex = 1;
	unsigned long vidx = 1;
	char* visited_vertices = NULL;
	char* joined_vertices = NULL;
	unsigned long vertex_count = grafo_num_vertices(graph);

	pq_t vertices_pq = pq_create(grafo_num_vertices(graph), PRIORITY_MINOR);
	Grafo* mst = grafo_crear(grafo_num_vertices(graph));

	visited_vertices = (char*) malloc(sizeof(char) * vertex_count);
	joined_vertices = (char*) malloc(sizeof(char) * vertex_count);

	memset(visited_vertices, 0, sizeof(char) * vertex_count);
	memset(joined_vertices, 0, sizeof(char) * vertex_count);

	while (vidx < vertex_count) {
		Iterador* it = NULL;
		unsigned long* data;

		if ('1' != visited_vertices[current_vertex - 1]) {
			visited_vertices[current_vertex - 1] = '1';
			it = grafo_adyacentes(graph, current_vertex);

			if (it) {

				do {
					data = malloc(sizeof(unsigned long) * 2);

					data[0] = current_vertex;
					data[1] = giter_vertice(it);

					pq_enqueue(vertices_pq, (void*) data, giter_peso(it));
				} while (giter_sig(it));

				free(it);
				data = NULL;
			}
		}

		data = (unsigned long*) pq_dequeue(vertices_pq);

		if (NULL != data
				&& !(joined_vertices[data[0] - 1]
						&& joined_vertices[data[1] - 1])) {
			grafo_agregar_arista(mst, data[0], data[1], 1);
			joined_vertices[data[0] - 1] = '1';
			joined_vertices[data[1] - 1] = '1';
			vidx++;
		}

		current_vertex = data[1];

		free(data);

		if (1 > pq_size(vertices_pq)) {
			unsigned short unvisited_vertices = 0;
			unsigned long unvisited_vertex = 0;
			unsigned long idx = 0;
			for (idx = 0; idx < vertex_count; idx++) {
				if ('1' != visited_vertices[idx]) {
					unvisited_vertices = 1;
					unvisited_vertex = idx + 1;
					break;
				}
			}

			if (!unvisited_vertices)
				break;
			else
				current_vertex = unvisited_vertex;

		}
	}

	pq_destroy(vertices_pq, __vertex_pq_data_destroy);
	free(visited_vertices);
	free(joined_vertices);
	return mst;

}

/********************************************************/
/* EL MAIN                                              */
/********************************************************/
/* 
 Este programa se ejecuta asi:

 proy2.exe 30 30 mi-laberinto.tga 

 Esto crea un laberinto 30x30 en un archivo mi-laberinto.tga
 (TGA es un formato de imagenes.. puedes cambiarlo a JPG con tu herramienta
 favorita o con IrfanView http://www.irfanview.com/ )

 Tambien puedes usar PPM que es un formato mas facil de depurar
 */
#define TILE_WIDTH 10
int main(int argc, char* argv[]) {
	cfg_params_t* params = malloc(sizeof(cfg_params_t));
	Grafo* graph = NULL;
	Grafo* mst = NULL;
	unsigned long cidx = 0;
	unsigned long ridx = 0;
	unsigned long idx = 0;
	Color line_color;
	Image* img = NULL;

	/* Procesa los argumentos del main */
	cfg_params_digest(argc, argv, params);

	/* Crea un grafo */
	graph = grafo_crear(params->columns * params->rows);

	for (cidx = 0; cidx < params->columns; cidx++) {
		for (ridx = 0; ridx < params->rows; ridx++) {
			srand(time(NULL) + cidx * ridx);
			unsigned long v0 = params->columns * ridx + cidx + 1;
			unsigned long v1 = v0 + 1;

			if (params->columns * (ridx + 1) >= v1)
				grafo_agregar_arista(graph, v0, v1, rand() % ULONG_MAX + 1);

			v1 = cidx + 1 + params->columns * (ridx + 1);

			if (params->columns * params->rows >= v1)
				grafo_agregar_arista(graph, v0, v1, rand() % ULONG_MAX + 1);
		}
	}

	/* Ahora usa tu cola de prioridades y grafo para correr el algoritmo de Prim */
	mst = prim(graph);

	/* Ahora recolecta tus aristas y dibuja en una imagen 2D */

	line_color.r = 255;
	line_color.g = 0;
	line_color.b = 0;

	img = CreateImage((params->rows - 1) * TILE_WIDTH + 1,
			(params->columns - 1) * TILE_WIDTH + 1);

	for (idx = 1; idx <= grafo_num_vertices(mst); idx++) {
		Iterador* it = grafo_adyacentes(mst, idx);

		if (it) {
			unsigned long vertex0 = 0;
			unsigned long col0 = 0;
			unsigned long row0 = 0;
			unsigned long vertex1 = 0;
			unsigned long col1 = 0;
			unsigned long row1 = 0;

			vertex0 = idx - 1;
			col0 = (vertex0 % params->columns) * TILE_WIDTH;
			row0 = (vertex0 / params->columns) * TILE_WIDTH;

			do {
				vertex1 = giter_vertice(it) - 1;
				col1 = (vertex1 % params->columns) * TILE_WIDTH;
				row1 = (vertex1 / params->columns) * TILE_WIDTH;

				DrawStraightLine(img, col0, row0, col1, row1, &line_color);
			} while (giter_sig(it));

			free(it);
		}

	}

	SavePPM(img, params->ofname);
	CONFIRM_NORETURN(DestroyImage(img));

	free(params);
	grafo_destruir(mst);
	grafo_destruir(graph);

	return 0;
}

