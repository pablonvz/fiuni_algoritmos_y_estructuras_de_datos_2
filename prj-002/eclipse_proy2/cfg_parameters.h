#ifndef __CFG_PARAMETERS_H
#define __CFG_PARAMETERS_H

#include <stdio.h>
#include <stdlib.h>

#include "confirm.h"

typedef struct {
	unsigned long columns;
	unsigned long rows;
	char* ofname;
} cfg_params_t;

void usage(char* exec_fname) {
	printf("%s <cols> <rows> <out file name>\n", exec_fname);
}

int cfg_params_digest(int argc, char** argv, cfg_params_t* st) {
	CONFIRM_RETVAL(NULL != st, 1);

	if (4 != argc) {
		usage(argv[0]);
		return 1;
	}

	st->columns = strtoul(argv[1], NULL, 10);
	st->rows = strtoul(argv[2], NULL, 10);
	st->ofname = argv[3];

	return 0;
}

#endif
