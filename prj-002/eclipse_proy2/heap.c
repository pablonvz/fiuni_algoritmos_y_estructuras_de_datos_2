#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pq.h"

typedef struct heap_node_st {
	long priority;
	void* data;
} heap_node_t;

typedef struct heap_st {
	heap_node_t** nodes;
	unsigned long size;
	unsigned long capacity;
	/* pcompare returns 0 if the priorities are in order */
	int (*pcompare)(long root, long child);
} heap_t;

int __mcompare(long proot, long pchild) {
	return proot <= pchild;
}

int __gcompare(long proot, long pchild) {
	return pchild >= pchild;
}

unsigned long heap_lchild(unsigned long idx) {
	return idx * 2;
}

unsigned long heap_rchild(unsigned long idx) {
	return idx * 2 + 1;
}

unsigned long heap_node_root(unsigned long idx) {
	return idx / 2;
}

void heap_swap_nodes(heap_t* heap, unsigned long idx0, unsigned long idx1) {
	heap_node_t* tmp = heap->nodes[idx0];
	heap->nodes[idx0] = heap->nodes[idx1];
	heap->nodes[idx1] = tmp;
}

void heap_percolate_up(heap_t* heap, unsigned long idx) {
	unsigned long pidx = heap_node_root(idx);
	long priority = heap->nodes[idx]->priority;
	long parent_priority = heap->nodes[pidx]->priority;

	if (heap->pcompare(parent_priority, priority))
		return;

	heap_swap_nodes(heap, pidx, idx);
	heap_percolate_up(heap, pidx);
}

void heapify(heap_t* heap) {
	unsigned long idx;

	for (idx = 2; idx < heap->size; idx++) {
		heap_percolate_up(heap, idx);
	}
}

void __fake_heap_node_data_destroy(void* arg0) {
	return;
}

/* priority queue functions */
pq_t pq_create(unsigned long cap, unsigned short ppref) {
	pq_t pq = NULL;
	heap_t* heap = NULL;
	heap_node_t** nodes = NULL;

	long sentinel_priority = 0;
	int (*pcompare)(long, long) = NULL;

	cap++;

	heap = (heap_t*) malloc(sizeof(heap_t));
	nodes = (heap_node_t**) malloc(sizeof(heap_node_t*) * cap);

	if (!heap || !nodes) {
		printf("Can not allocate memory to create the priority queue.");
		free(heap);
		free(nodes);
	}

	memset(heap, 0, sizeof(heap_t));
	memset(nodes, 0, sizeof(heap_node_t*) * cap);

	switch (ppref) {
	case PRIORITY_GREATER:
		pcompare = __gcompare;
		sentinel_priority = LONG_MAX;
		break;
	case PRIORITY_MINOR:
		pcompare = __mcompare;
		sentinel_priority = LONG_MIN;
		break;
	default:
		printf("Unknown priority preference. Using PRIORITY_MINOR as default.");
		pcompare = __mcompare;
		sentinel_priority = LONG_MIN;
	}

	heap->capacity = cap;
	heap->nodes = nodes;
	heap->size = 0;
	heap->pcompare = pcompare;

	pq = (pq_t) heap;

	if (!pq_enqueue(pq, NULL, sentinel_priority))
		pq_destroy(pq, __fake_heap_node_data_destroy);

	return pq;
}

pq_t pq_create_from_array(unsigned short ppref, unsigned long size,
		void** values, unsigned long* priorities) {
	unsigned long idx = 0;
	pq_t pq = pq_create(size, ppref);

	for (idx = 0; idx < size; idx++)
		pq_enqueue(pq, values[idx], priorities[idx]);

	return pq;
}

unsigned short pq_enqueue(pq_t pq, void* value, long priority) {
	heap_t* heap = (heap_t*) pq;
	heap_node_t* node = NULL;

	node = (heap_node_t*) malloc(sizeof(heap_node_t));

	node->data = value;
	node->priority = priority;

	heap->nodes[heap->size++] = node;

	heap_percolate_up(heap, heap->size - 1);

	return 1;
}

void* pq_dequeue(pq_t pq) {
	heap_t* heap = (heap_t*) pq;
	void* data = NULL;

	if (heap->size == 1)
		return data;

	data = heap->nodes[1]->data;

	free(heap->nodes[1]);

	heap->size--;
	heap->nodes[1] = heap->nodes[heap->size];
	heap->nodes[heap->size] = NULL;

	heapify(heap);

	return data;
}

unsigned long pq_size(pq_t pq) {
	return ((heap_t*) pq)->size - 1;
}

void pq_destroy(pq_t pq, void (*pq_node_data_destroy)(void*)) {
	heap_t* heap = (heap_t*) pq;
	unsigned long idx = 0;

	if (heap) {
		for (idx = 1; idx < heap->size; idx++) {
			pq_node_data_destroy(heap->nodes[idx]->data);
			free(heap->nodes[idx]);
		}

		free(heap->nodes[0]);
		free(heap->nodes);
		free(heap);
	}
}
