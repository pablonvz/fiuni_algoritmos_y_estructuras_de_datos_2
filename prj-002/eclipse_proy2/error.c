#include <stdio.h>

#include "confirm.h"
/*
 Esta funcion es requerida por confirm.h y su proposito es 
 establecer lo que sucede cuando hay un error inesperado.
 */

extern void GlobalReportarError(char* pszFile, int iLine) {

	/* Siempre imprime el error */
	fprintf(
	stderr, "\nERROR NO ESPERADO: en el archivo %s linea %u", pszFile, iLine);

}
