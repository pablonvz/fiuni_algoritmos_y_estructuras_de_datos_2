Informe

El proyecto lo hice en las siguientes etapas.
 - Investigué como convertir string a números, escribí la función correspondiente
para convertir los vectores en el archivo a vectores de números en memoria.
 - Cargar los vectores del archivo de entrenamiento en memoria y verificar que
estos sean cargados correctamente.
 - PROBLEMA 00: por alguna razón no podía leer los archivos (utilizando la ruta completa al archivo).
 - SOLUCION 00: utilizando las rutas relativas a los archivos la lectura de los mismos
se realiza sin problemas.
 - PROBLEMA 01: al leer el archivo como texto, no cargaba los caracteres de fin de línea.
 - SOLUCION 01: leer los archivos como binarios.
 - PROBLEMA 02: darme cuenta de que el fín de línea en DOS se representa con dos caracteres
(\r\n) en lugar de solo uno como en linux (\n)
 - Cargar los vectores del archivo de pruebas en memoria
 - Calcular la distancia euclediana de cada vector de prueba con cada vector de entrenamiento.
 - Ordenar las distancias de menor a mayor.
 - Realizar la "votación" con los K menores distancias.
 
Realicé pruebas para hallar el valor óptimo de K con los valores 15, 25, 50, 75,
100, 125, 150, 175 y 200. Obteniendo mejor resultado con valores pequeños de K.

Con K = 15 obtuve un 96.8% de éxito, mientras que con K = 25 obtuve 96.6%
El porcentaje de éxito va decayendo progresivamente a medida que aumenta el
valor de K.