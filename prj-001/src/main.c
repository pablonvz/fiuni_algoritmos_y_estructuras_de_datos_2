#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "prj001util.h"

#define TRA_VECTOR_COUNT 3823
#define TES_VECTOR_COUNT 1797
#define VECTOR_LEN 65
#define K 15

/*
    if DEBUG is defined, print the index of the vector when the
    verification digit (last vector's value) not equal to the found digit
*/
//#define DEBUG

int main(int argc, char** argv) {
    // variables declaration
    options_t opts = parse_arg(argc, argv);
    FILE* ftra = NULL;
    FILE* ftes = NULL;

    uint32_t vtes_idx = 0;
    uint32_t vtra_idx = 0;
    uint32_t idx = 0;

    uint32_t* tra_vectors = NULL;
    uint32_t* tes_vectors = NULL;

    vdistance_t* distances = NULL;
    uint32_t votes[10];
    uint32_t test_count[10];
    uint32_t success_count[10];

    // variables initialization
    if (opts.training_fname != NULL && opts.testing_fname != NULL) {
        ftra = fopen(opts.training_fname, "rb");
        ftes = fopen(opts.testing_fname, "rb");

        rewind(ftra);
        rewind(ftes);
    } else {
        printf("Help:\n\t<program_name> --tra path/to/digits.tra --tes path/to/digits.tes");
        return -1;
    }

    tra_vectors = (uint32_t*) malloc(sizeof(uint32_t) * TRA_VECTOR_COUNT * VECTOR_LEN);
    tes_vectors = (uint32_t*) malloc(sizeof(uint32_t) * TES_VECTOR_COUNT * VECTOR_LEN);
    distances = (vdistance_t*) malloc(sizeof(vdistance_t) * TRA_VECTOR_COUNT);

    if (!(tra_vectors && tes_vectors && distances)) {
        if (tra_vectors) free(tra_vectors);
        if (tes_vectors) free(tes_vectors);
        if (distances) free(distances);

        printf("There is an error allocating memory");
        return -1;
    }

    memset((void*) tra_vectors, 0, sizeof(uint32_t) * TRA_VECTOR_COUNT * VECTOR_LEN);
    memset((void*) tes_vectors, 0, sizeof(uint32_t) * TES_VECTOR_COUNT * VECTOR_LEN);
    memset((void*) distances, 0, sizeof(vdistance_t) * TRA_VECTOR_COUNT);

    memset(votes, 0, sizeof(uint32_t) * 10);
    memset(test_count, 0, sizeof(uint32_t) * 10);
    memset(success_count, 0, sizeof(uint32_t) * 10);

    // project kernel
    load_vectors(tra_vectors, ftra, TRA_VECTOR_COUNT, VECTOR_LEN);
    load_vectors(tes_vectors, ftes, TES_VECTOR_COUNT, VECTOR_LEN);

    for (vtes_idx = 0; vtes_idx < TES_VECTOR_COUNT; vtes_idx++) {
        uint32_t* vtes = tes_vectors + (vtes_idx * VECTOR_LEN);
        uint32_t __tmpdigits[2] = { vtes[VECTOR_LEN - 2], vtes[VECTOR_LEN - 1]};
        uint32_t digit = 0;

        for (vtra_idx = 0; vtra_idx < TRA_VECTOR_COUNT; vtra_idx++) {
            uint32_t* vtra = tra_vectors + (vtra_idx * VECTOR_LEN);

            vdistance_t* dstr = &distances[vtra_idx];

            distances[vtra_idx].distance = euclidean_distance(vtra, vtes, VECTOR_LEN - 1);
            distances[vtra_idx].digit = vtra[VECTOR_LEN - 1];
        }

        qsort(distances, TRA_VECTOR_COUNT, sizeof(vdistance_t), vdistance_compare);

        memset(votes, 0, sizeof(uint32_t) * 10);
        vote(votes, distances, K);

        for (idx = 0; idx < 10; idx++)
            digit = votes[idx] > votes[digit] ? idx : digit;

        #ifdef DEBUG
        if (vtes[VECTOR_LEN-1] != digit)
            printf("vidx: %i\t vdigit: %i\t digit: %i\n",
                vtes_idx, vtes[VECTOR_LEN-1], digit);
        #endif

        test_count[vtes[VECTOR_LEN - 1]] += 1;

        if (vtes[VECTOR_LEN - 1] == digit)
            success_count[vtes[VECTOR_LEN - 1]] += 1;
    }

    // print both sample size (K) and success percent of each digit

    printf("Sample size (K): %i\n", K);

    for (idx = 0; idx < 10; idx++)
        printf("Digit: %i \tSuccess: %i%%\n", idx,
            (int) (((float) success_count[idx] / (float) test_count[idx]) * 100));

    fclose(ftra);
    fclose(ftes);

    free(distances);
    free(tra_vectors);
    free(tes_vectors);

    return 0;
}