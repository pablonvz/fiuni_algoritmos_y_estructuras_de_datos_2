#ifndef PRJ001UTIL_H_
#define PRJ001UTIL_H_

#include <stdio.h>
#include <stdint.h>

typedef struct {
    char* training_fname;
    char* testing_fname;
} options_t;

typedef struct {
    uint64_t distance;
    uint32_t digit;
} vdistance_t;

options_t parse_arg(int argc, char** argv);

options_t create_empty_options();

uint32_t fgetuint(FILE* stream, char delimiter);

uint32_t load_vectors(uint32_t* vectors, FILE* f, uint32_t vcount, uint32_t vlen);

uint64_t euclidean_distance(uint32_t* v0, uint32_t* v1, uint32_t vlen);

/* used by qsort */
int vdistance_compare(const void* d0, const void* d1);

void vote(uint32_t* votes, vdistance_t* distances, uint32_t k);

#endif
