#include "prj001util.h"

#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

options_t parse_arg(int argc, char** argv) {
    options_t opts = create_empty_options();
    int idx;

    for (idx = 1; idx < argc; idx++) {
        char* arg = argv[idx];

        if (0 == strcmp("--tra", arg))
            opts.training_fname = argv[++idx];

        if (0 == strcmp("--tes", arg))
            opts.testing_fname = argv[++idx];
    }

    return opts;
}


options_t create_empty_options() {
    options_t opts;

    opts.training_fname = NULL;
    opts.testing_fname = NULL;

    return opts;
}


#ifndef BUFFER_LEN
#define BUFFER_LEN sizeof(uint32_t) * CHAR_BIT
#endif

uint32_t fgetuint(FILE* stream, char delimiter) {
    char fbuffer[BUFFER_LEN];
    char nbuffer[BUFFER_LEN];

    uint32_t idx = 0;
    uint32_t nbuffer_idx = 0;

    uint64_t forigin_pos = 0;

    if (NULL == stream || feof(stream))
        return 0;

    forigin_pos = ftell(stream);

    memset((void*) fbuffer, 0, BUFFER_LEN);
    memset((void*) nbuffer, 0, BUFFER_LEN);

    fread(fbuffer, sizeof(char), BUFFER_LEN, stream);

    for (idx = 0; idx < BUFFER_LEN; idx++) {
        char c = fbuffer[idx];

        if (c > ('0' - 1) && c < ('9' + 1)) {
            nbuffer[nbuffer_idx++] = fbuffer[idx];
            continue;
        } else if (c == '\r' && fbuffer[idx + 1] == '\n') {
            /* check for DOS' end of line style and increases idx
            to avoid read both characters separately */
            idx++;
            break;
        } else
            break;
    }

    fseek(stream, forigin_pos + sizeof(char) * idx + 1, SEEK_SET);
    return atoi(nbuffer);
}

#undef BUFFER_LEN


uint32_t load_vectors(uint32_t* vectors, FILE* f, uint32_t vcount, uint32_t vlen) {
    uint32_t vidx = 0;
    uint32_t eidx = 0;

    for (vidx = 0; vidx < vcount; vidx++) {
        for (eidx = 0; eidx < vlen; eidx++) {
            uint32_t idx = vlen * vidx + eidx;
            uint32_t uint = fgetuint(f, ','); 

            vectors[idx] = uint;
        }
    }

    return 0;
}

uint64_t euclidean_distance(uint32_t* v0, uint32_t* v1, uint32_t vlen) {
    uint64_t distance = 0;
    uint32_t idx = 0;

    for (idx = 0; idx < vlen; idx++) {
        uint32_t ev0 = v0[idx];
        uint32_t ev1 = v1[idx];

        distance += (uint64_t) pow(abs((double)ev0 - ev1), 2.0);
    }

    return (uint64_t) sqrt((long double) distance);
}

int vdistance_compare(const void* d0, const void* d1) {
    vdistance_t dstr0 = *(vdistance_t*) d0;
    vdistance_t dstr1 = *(vdistance_t*) d1;

    return dstr0.distance - dstr1.distance;
}

void vote(uint32_t* votes, vdistance_t* distances, uint32_t k) {
    uint32_t didx;

    for (didx = 0; didx < k; didx++) {
        vdistance_t dstr = distances[didx];

        votes[dstr.digit] += 1;
    }
}